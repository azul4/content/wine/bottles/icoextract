# icoextract

Icon extractor for Windows PE files (.exe/.dll) with optional thumbnailer functionality

https://github.com/jlu5/icoextract

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/wine/bottles/icoextract.git
```

